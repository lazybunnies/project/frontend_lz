import { createRouter, createWebHistory } from 'vue-router'
import LoginView from "@/views/LoginView.vue";
import HomeView from '../views/HomeView.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        default: HomeView,
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/login",
      name: "login",
      components: {
        default: LoginView,
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/report",
      name: "report",

      components: {
        default: () => import("../views/reports/ReportView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
        requiresRole: ["Owner"],
      },
    },
    {
      path: "/table",
      name: "table",

      components: {
        default: () => import("../views/tabels/TableView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
        requiresRole: ["Receptionist","Owner","Manager"],
      },
    },
    {
      path: "/food",
      name: "food",

      components: {
        default: () => import("../views/menus/FoodView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
        // requiresAuth: true,
      },
    },
    {
      path: "/cook",
      name: "cook",

      components: {
        default: () => import("../views/cooks/CookView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
        requiresRole: ["Chef","Owner","Manager"],
      },
    },
    {
      path: "/serve",
      name: "serve",

      components: {
        default: () => import("../views/serves/ServeView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
        requiresRole: ["Waitress","Owner","Manager"],
      },
    },
    {
      path: "/payment",
      name: "payment",

      components: {
        default: () => import("../views/tabels/PayView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
        requiresRole: ["Cashier","Owner","Manager"],
      },
    },
    {
      path: "/cleaning",
      name: "cleaning",

      components: {
        default: () => import("../views/tabels/CleanView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
        requiresRole: ["Cleaning","Owner","Manager"],
      },
    },
    {
      path: "/menu",
      name: "menu",

      components: {
        default: () => import("../views/menus/MenuView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
        requiresRole: ["Manager","Owner"],
      },
    },
    {
      path: "/employee",
      name: "employee",

      components: {
        default: () => import("../views/backends/UserView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
        requiresRole: "Owner",
      },
    },
    {
      path: "/salary",
      name: "salary",

      components: {
        default: () => import("../views/backends/SalaryView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
        requiresRole: "Owner",
      },
    },
    {
      path: "/stock",
      name: "stock",

      components: {
        default: () => import("../views/stocks/AllStockView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
        requiresRole: ["Backend","Owner","Manager"],
      },
    },
    {
      path: "/listtable",
      name: "listtable",

      components: {
        default: () => import("../views/backends/ListTableView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
        requiresRole: "Owner",
      },
    },
    {
      path: "/check",
      name: "check",

      components: {
        default: () => import("../views/CheckView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
  ]
});

function isLogin() {
  const user = localStorage.getItem("user");
  if (user) {
    return true;
  }
  return false;
}

router.beforeEach((to, from, next) => {
  const roles = to.meta.requiresRole as string[];
  const user = JSON.parse(localStorage.getItem('user') || '{}')
  const userRole = user.type;
  console.log(roles)
  console.log(userRole)
  if (to.meta.requiresAuth && !isLogin()) {
    next('/login')
  } else if (to.meta.requiresRole) {   
    console.log(userRole)
    if (!userRole || !roles.includes(userRole)) {
      next('/')
    }
  }
  return next()
})

// router.beforeEach((to, from,) => {
//   const authStore = useAuthStore();
//   // instead of having to check every route record with
//   // to.matched.some(record => record.meta.requiresAuth)
//   if (to.meta.requiresAuth && !isLogin()) {

//     return {
//       path: "/login",
//       query: { redirect: to.fullPath },
//     };
//   }
//   // else if ((to.meta.requiresRole && authStore.role != to.meta.requiresRole) && 
//   // (to.meta.requiresRoleO && authStore.roleO != to.meta.requiresRoleO) &&
//   // (to.meta.requiresRoleM && authStore.roleM != to.meta.requiresRoleM)) {
//   //   console.log(to.meta.requiresRole)
//   //   console.log(to.meta.requiresRoleO)
//   //   console.log(to.meta.requiresRoleM)
//   //   console.log(authStore.role)
//   //   console.log(authStore.roleO)
//   //   console.log(authStore.roleM)
//   //   console.log(to.meta.requiresRole && authStore.role != to.meta.requiresRole)
//   //   console.log(to.meta.requiresRoleO && authStore.roleO != to.meta.requiresRoleO)
//   //   console.log(to.meta.requiresRoleM && authStore.roleM != to.meta.requiresRoleM)
//   //   // ถ้าเข้าถึงหน้าที่ต้องการสิทธิ์ที่ไม่ตรงกับ role ที่มี ให้ redirect ไปยังหน้าที่เหมาะสม
//   //   // if (authStore.role == "Cashier") {
//   //   //   return {
//   //   //     path: "/payment",
//   //   //     query: { redirect: to.fullPath },
//   //   //   };
//   //   // } else if (authStore.role == "Receptionist") {
//   //   //   return {
//   //   //     path: "/food",
//   //   //     query: { redirect: to.fullPath },
//   //   //   };
//   //   // } else if (authStore.role == "Cleaning") {
//   //   //   return {
//   //   //     path: "/cleaning",
//   //   //     query: { redirect: to.fullPath },
//   //   //   };
//   //   // } 
//   //   // else {
//   //   //   return {
//   //   //     path: "/",
//   //   //     query: { redirect: to.fullPath },
//   //   //   };
//   //   // }
//   //   return {
//   //     path: "/",
//   //     // query: { redirect: to.fullPath },
//   //   };
//   // }
// });

export default router
