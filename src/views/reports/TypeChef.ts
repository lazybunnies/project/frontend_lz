export default interface TypeChef {
  id?: number;
  fname: string;
  lname: string;
  count: number;
}