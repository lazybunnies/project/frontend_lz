export default interface TypeTop {
  id?: number;
  name: string;
  sum: number;
}