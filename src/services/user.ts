import type User from "@/types/User";
import http from "./axios";
function getUsers() {
  return http.get("/users");
}

function getUserByName(name:string){
  return http.get(`/users/name/${name}`);
}

// function saveUser(user: User) {
//   return http.post("/users", user);
// }

function saveUser(user: User & {files: File[]}) {
  const formDate = new FormData();
  formDate.append("fname",user.fname)
  formDate.append("lname",user.lname)
  formDate.append("type",user.type)
  formDate.append("rate",`${user.rate}`)
  formDate.append("gender",user.gender)
  formDate.append("tel",user.tel)
  if(user.files){
  formDate.append("file",user.files[0])
  }
  formDate.append("username",user.username)
  formDate.append("password",user.password)
  return http.post("/users", formDate, {headers: {
    "Content-Type": "multipart/form-data",
  }});
}

// function updateUser(id: number, user: User) {
//   return http.patch(`/users/${id}`, user);
// }

function updateUser(id: number, user: User & {files: File[]}) {
  const formDate = new FormData();
  formDate.append("fname",user.fname)
  formDate.append("lname",user.lname)
  formDate.append("type",user.type)
  formDate.append("rate",`${user.rate}`)
  formDate.append("gender",user.gender)
  formDate.append("tel",user.tel)
  if(user.files){
  formDate.append("file",user.files[0])
  }
  formDate.append("username",user.username)
  formDate.append("password",user.password)
  return http.patch(`/users/${id}`, formDate, {headers: {
    "Content-Type": "multipart/form-data",
  }});
}

function deleteUser(id: number) {
  return http.delete(`/users/${id}`);
}

export default { getUsers, saveUser, updateUser, deleteUser, getUserByName};
