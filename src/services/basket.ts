import type Basket from "@/types/Basket";
import type BasketAdd from "@/types/BasketAdd";
import http from "./axios";

function getBasketByID(order: string,id: number){
  return http.get(`/baskets/${order}/${id}`);
}

function getBasketByOrder(order: string){
  return http.get(`/baskets/${order}`);
}
function saveBasket(basket: BasketAdd){
  return http.post("/baskets", basket);
}
function updateBasket(id: number, basket: BasketAdd){
  return http.patch(`/baskets/${id}`, basket);
}
function deleteBasket(order: string,id: number){
  return http.delete(`/baskets/${order}/${id}`);
}

export default {getBasketByOrder,saveBasket,updateBasket,deleteBasket,getBasketByID};