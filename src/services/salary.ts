import type Salary from "@/types/Salary";
import http from "./axios";
function getSalary() {
  return http.get("/salary");
}
function saveSalary(salary: Salary) {
  return http.post("/salary", salary);
}
function updateSalary(id: number, salary: Salary) {
  return http.patch(`/salary/${id}`, salary);
}
function deleteSalary(id: number) {
  return http.delete(`/salary/${id}`);
}

export default { getSalary ,deleteSalary,updateSalary,saveSalary };
