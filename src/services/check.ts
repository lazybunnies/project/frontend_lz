import type Check from "@/types/Check";
import type CheckAdd from "@/types/CheckAdd";
import type Salary from "@/types/Salary";
import http from "./axios";
function getCheck() {
  return http.get("/check");
}
function saveCheck(check: CheckAdd) {
  return http.post("/check", check);
}
function updateCheck(id: number, check: CheckAdd) {
  return http.patch(`/check/${id}`, check);
}
function deleteCheck(id: number) {
  return http.delete(`/check/${id}`);
}

export default { getCheck,saveCheck,updateCheck,deleteCheck} ;
