import type CheckStock from "@/types/CheckStock";
import type CheckStockList from "@/types/CheckStockList";
import http from "./axios";

function getCheckStock() {
  return http.get("/checkStocks");
}
function getCheckStockByDate(createdAt: string) {
  return http.get(`check-stock-list/createdAt/${createdAt}`)
}
function saveCheckStock(CheckStock: CheckStock) {
  return http.post("/checkStocks", CheckStock);
}
function getCheckStockList() {
  return http.get("/check-stock-list");
}

function saveCheckStockList(CheckStockList: CheckStockList) {
  return http.post("/check-stock-list", CheckStockList);
}

function getStockkDetail(id: number) {
  return http.get(`/check-stock-detail/${id}`);
}

function saveCheckStockDetail(CheckStock: CheckStock) {
  return http.post("/check-stock-detail", CheckStock);
}

function updateCheckStock(id: number, CheckStock: CheckStock) {
  return http.patch(`/checkStocks/${id}`, CheckStock);
}
function updateCheckStockList(id: number, CheckStockList: CheckStockList) {
  return http.patch(`/checkStockLists/${id}`, CheckStockList);
}
function deleteCheckStock(id: number) {
  return http.delete(`/checkStocks/${id}`);
}
export default { getStockkDetail,getCheckStockList,getCheckStock, saveCheckStock, updateCheckStock, deleteCheckStock, getCheckStockByDate, updateCheckStockList, saveCheckStockList, saveCheckStockDetail };