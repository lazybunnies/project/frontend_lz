import type StockBuy from "@/types/StockBuy";
import type StockBuyList from "@/types/StockBuyList";

import http from "./axios";

function getStockBuy() {
  return http.get("/stockBuy");
}
function getStockBuyByDate(createdAt: string) {
  return http.get(`stock-buy-list/createdAt/${createdAt}`)
}
function getStockBuyByName(name: string) {
  return http.get(`/stockBuy/name/${name}`);
}
function saveStockBuy(StockBuy: StockBuy) {
  return http.post("/stockBuy", StockBuy);
}
function getStockBuyList() {
  return http.get("/stock-buy-list");
}

function saveStockBuyList(StockBuyList: StockBuyList) {
  return http.post("/stock-buy-list", StockBuyList);
}

function getStockBuyDetail(id: number) {
  return http.get(`/stock-buy-detail/${id}`);
}

function saveStockBuyDetail(StockBuy: StockBuy) {
  return http.post("/stock-buy-detail", StockBuy);
}

function updateStockBuy(id: number, StockBuy: StockBuy) {
  return http.patch(`/stockBuy/${id}`, StockBuy);
}
function updateStockBuyList(id: number, StockBuyList: StockBuyList) {
  return http.patch(`/stockBuyList/${id}`, StockBuyList);
}
function deleteStockBuy(id: number) {
  return http.delete(`/stockBuy/${id}`);
}
export default { getStockBuy,getStockBuyByDate,getStockBuyByName,saveStockBuy, getStockBuyList, saveStockBuyList, getStockBuyDetail, saveStockBuyDetail, updateStockBuy, updateStockBuyList, deleteStockBuy };