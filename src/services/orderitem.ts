
import type OrderitemAdd from "@/types/OrderitemAdd";
import http from "./axios";

// function getOrderItem(){
//   return http.get("/orderitems");
// }

function getBasketByOrder(order: string){
  return http.get(`/orderitems/${order}`);
}
function getOrderItemByStatus(text: string){
  return http.get(`/orderitems/status/${text}`);
}
function saveOrderItem(item: OrderitemAdd){
  return http.post("/orderitems", item);
}
function updateOrderItem(id: number, table: OrderitemAdd){
  return http.patch(`/orderitems/${id}`, table);
}
function deleteOrderItem(id: number){
  return http.delete(`/orderitems/${id}`);
}
export default {getBasketByOrder,saveOrderItem,updateOrderItem,deleteOrderItem,getOrderItemByStatus};