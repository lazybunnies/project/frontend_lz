import type Order from "@/types/Order";
import type OrderUpdate from "@/types/OrderUpdate";
import http from "./axios";

function getOrder() {
  return http.get("/orders");
}
function getOrderbyid(id: number) {
  return http.get(`/orders/${id}`);
}

function getOrderByUuid(uuid: string) {
  return http.get(`/orders/uuid/${uuid}`);
}
function saveOrder(order: OrderUpdate) {
  return http.post("/orders", order);
}
function updateOrder(id: number, order: OrderUpdate) {
  return http.patch(`/orders/${id}`, order);
}
function deleteOrder(id: number) {
  return http.delete(`/orders/${id}`);
}

function getOrderBytable(id: number){
  return http.get(`/orders/table/${id}`);
}

export default { getOrder ,deleteOrder,updateOrder,saveOrder,getOrderBytable,getOrderbyid,getOrderByUuid};