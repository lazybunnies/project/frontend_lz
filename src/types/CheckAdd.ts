export default interface CheckAdd {

  id?: number;


  user: number;


  isLate: boolean;


  checkIn: String;  

  checkOut: String;  


  createdAt?: Date;


  updatedAt?: Date;


  deletedAt?: Date;

}