export default interface Stock {
    id?: number;
    name?: string;
    minimum?: number;
    remaining?: number;
    unit?: string;
    price?: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}






