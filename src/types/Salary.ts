import type User from "./User";

export default interface Salary {
  id?: number;
  bonus: number;
  deduct: number;
  work: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  user?: User

}