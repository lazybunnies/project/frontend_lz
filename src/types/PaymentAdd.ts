export default interface PaymentAdd {
  id?: number;
  price: number;
  received: number;
  moneyChange: number;
  netPrice: number;
  discount: number;
  order: number;
  user: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}