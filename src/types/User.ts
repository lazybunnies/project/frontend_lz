export default interface User {
  id?: number;
  fname: string;
  lname: string;
  type: string;
  rate: number;
  gender: string;
  tel: string;
  image: string;
  username: string;
  password: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
