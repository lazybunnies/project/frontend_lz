import type User from "./User";

export default interface CheckStockList {
    id?: number;
    user?: User;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}






