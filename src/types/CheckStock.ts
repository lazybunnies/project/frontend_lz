import type CheckStockList from "./CheckStockList";
import type User from "./User";

export default interface CheckStock {
    id?: number;
    name?: string;
    // minimum: number;
    remaining?: number;
    unit?: string;
    price?: number;
    check?: number;
    // user?: User;
    checkStockList?: CheckStockList;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}






