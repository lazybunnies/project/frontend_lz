export default interface Menu {
  id?: number;
  name: string;
  price: number;
  type: string;
  status: string;
  image: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}