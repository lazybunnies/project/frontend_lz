export default interface OrderUpdate {
  id?: number;
  date?: Date;
  status: string;
  table_id: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}