export default interface Table{
  id?: number;
  type: string;
  Number: number;
  status: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}