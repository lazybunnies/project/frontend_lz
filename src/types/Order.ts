import type Table from "./ListTable";
export default interface Order {
  id?: number;
  date?: Date;
  status: string;
  table_id: Table;
  uuid?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}