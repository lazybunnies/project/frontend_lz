import type Menu from "./Menu";

export default interface Basket {
  id?: number;
  amount: number;
  note: string;
  order: string;
  menu: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}