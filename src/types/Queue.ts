import type Orderitem from "./Orderitem";
import type User from "./User";

export default interface Queue {
  id?: number;
  status: string;
  orderitem: Orderitem;
  emp_cook: User;
  emp_serve: User;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}