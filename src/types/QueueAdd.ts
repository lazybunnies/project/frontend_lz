export default interface QueueAdd {
  id?: number;
  status: string;
  orderitem: number;
  emp_cook?: number;
  emp_serve?: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}