import type Menu from "./Menu";
import type Order from "./Order";

export default interface OrderitemAdd {
  id?: number;
  amount: number;
  note: string;
  // status: string;
  numCancel?: number;
  numConfirm?: number;
  menu: number;
  order: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}