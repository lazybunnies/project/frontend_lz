import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Check from "@/types/Check";
import checkService from "@/services/check";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type CheckAdd from "@/types/CheckAdd";

export const useCheckStore = defineStore("Check", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const check = ref<Check[]>([]);
  const editedCheck = ref<CheckAdd>( {user: 0, isLate: false ,checkIn: "" ,checkOut: ""} );
  const userId = ref(0)

  function clean(){
    editedCheck.value = ( {user: 0, isLate: false ,checkIn: "" ,checkOut: ""} )
  }

  async function getCheck() {
    try {
      const res = await checkService.getCheck();
      check.value = res.data;
      console.log(res)
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Check ได้");
    }
  }
  async function deleteCheck(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await checkService.deleteCheck(id);
      await getCheck();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Check ได้");
    }
    loadingStore.isLoading = false;
  }
  
  async function saveCheck(checkid: number,time: string , user:number) {
    loadingStore.isLoading = true;
    try {
      editedCheck.value.id = checkid;
      editedCheck.value.checkOut = time;
      editedCheck.value.user = user;
      if (editedCheck.value.id) {
        const res = await checkService.updateCheck(
          editedCheck.value.id,
          editedCheck.value
        );
      } 
      //else {
      //   const res = await checkService.saveCheck(editedCheck.value);
      // }
      dialog.value = false;
      // clearProduct();
      await getCheck();
    }catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล product ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
    clean()
  }

  async function checkIn(user: number,time: string) {
    
    loadingStore.isLoading = true;
    try {
      editedCheck.value.checkIn = time;
      editedCheck.value.user = user;
      const res = await checkService.saveCheck(editedCheck.value);
      dialog.value = false;
      // clearProduct();
      await getCheck();
    }catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล product ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
    clean()
  }



  function editCheck(check: Check) {
    editedCheck.value = JSON.parse(JSON.stringify(check));
    dialog.value = true;
  }
  return {
    getCheck,
    check,
    deleteCheck,
    editCheck,
    editedCheck,
    saveCheck,
    dialog,
    checkIn,
    userId
  };
});
