import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/services/user";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";


export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const deleteDialog = ref(false);
  const search = ref("");
  const dID = ref();
  const dFname = ref();
  const dLname = ref();
  const users = ref<User[]>([]);
  const editedUser = ref<User & { files: File[]}>({ fname: "", lname: "",type: "",rate: 0 ,gender: "" ,tel: "" ,image: "No_Image_Available.png" ,files: [],username: "",password: ""});

  function dialogDelete(item: User){
    dID.value = item.id;
    dFname.value = item.fname;
    dLname.value = item.lname;
    deleteDialog.value = true
  }

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedUser.value = { fname: "", lname: "",type: "",rate:0 ,gender: "",tel: "",image: "No_Image_Available.png" ,files: [],username: "",password: "" };
    }
  });
  async function getUsers() {
    // loadingStore.isLoading = true;
    try {
      if(search.value == ''){
        const res = await userService.getUsers();
        users.value = res.data;
      }else{
        const res = await userService.getUserByName(search.value);
        users.value = res.data;
        // const [resFName, resLName] = await Promise.all([
        //   userService.getUserByFName(search.value),
        //   userService.getUserByLName(search.value)
        // ]);
        // users.value = [...resFName.data, ...resLName.data];
      }
      
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
    }
    // loadingStore.isLoading = false;
  }

  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        const res = await userService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await userService.saveUser(editedUser.value);
      }

      dialog.value = false;
      await getUsers();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก User ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteUser(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await userService.deleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ User ได้");
    }
    deleteDialog.value = false;
    loadingStore.isLoading = false;
  }
  function editUser(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }

  return {
    users,
    getUsers,
    dialog,
    editedUser,
    saveUser,
    editUser,
    deleteUser,
    deleteDialog,
    dialogDelete,
    dID,
    dFname,
    dLname,search
  };
});
