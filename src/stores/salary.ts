import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Salary from "@/types/Salary";
import salaryService from "@/services/salary";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useSalaryStore = defineStore("Salary", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const salary = ref<Salary[]>([]);
  const editedSalary = ref<Salary>( {bonus: 0,deduct: 0,work: 0,} );

  async function getSalary() {
    try {
      const res = await salaryService.getSalary();
      salary.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Salary ได้");
    }
  }
  async function deleteSalary(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await salaryService.deleteSalary(id);
      await getSalary();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Salary ได้");
    }
    loadingStore.isLoading = false;
  }
  
  async function saveSalary() {
    loadingStore.isLoading = true;
    try {
      if (editedSalary.value.id) {
        const res = await salaryService.updateSalary(
          editedSalary.value.id,
          editedSalary.value
        );
      } else {
        const res = await salaryService.saveSalary(editedSalary.value);
      }

      dialog.value = false;
      // clearProduct();
      await getSalary();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Salary ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editSalary(salary: Salary) {
    editedSalary.value = JSON.parse(JSON.stringify(salary));
    dialog.value = true;
  }
  return {
    getSalary,
    salary,
    deleteSalary,
    editSalary,
    editedSalary,
    saveSalary,
    dialog,
  };
});
