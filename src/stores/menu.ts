import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/types/Menu";
import menuService from "@/services/menu";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";


export const useMenuStore = defineStore("Menu", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const deleteDialog = ref(false);
  const search =ref("");
  const menus = ref<Menu[]>([]);
  const menu = ref<Menu>();
  const editedMenu = ref<Menu & { files: File[]}>({ name: "", price: 0 ,type: "",status: "on",image: "No_Image_Available.png",files: []});

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedMenu.value = { name: "", price: 0 ,type: "",status: "on",image: "No_Image_Available.png" ,files: []};
    }
  });

  async function getMenus() {
    try {
      if(search.value == ''){
        const res = await menuService.getMenus();
        menus.value = res.data;
      }else{
        const res = await menuService.getMenuByName(search.value);
        menus.value = res.data;
      }
      
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Menu ได้");
    }
  }

  async function getMenusByName(name:string) {
    try {
      const res = await menuService.getMenuByName(name);
      menus.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Menu ได้");
    }
  }


  async function saveMenu() {
    loadingStore.isLoading = true;
    try {
      if (editedMenu.value.id) {
        const res = await menuService.updateMenu(
          editedMenu.value.id,
          editedMenu.value
        );
        console.log(editedMenu.value)
      } else {
        const res = await menuService.saveMenu(editedMenu.value);
      }

      dialog.value = false;
      await getMenus();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Menu ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteMenu(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await menuService.deleteMenu(id);
      await getMenus();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Menu ได้");
    }
    deleteDialog.value = false;
    loadingStore.isLoading = false;
  }
  function editMenu(menu: Menu) {
    editedMenu.value = JSON.parse(JSON.stringify(menu));
    dialog.value = true;
  }
  function switchMenu(menu: Menu){
    editedMenu.value = JSON.parse(JSON.stringify(menu));
    if(editedMenu.value.status == 'on'){
      editedMenu.value.status='off'
    }else{
      editedMenu.value.status='on'
    }
    saveMenu();
    editedMenu.value = { name: "", price: 0 ,type: "",status: "on",image: "No_Image_Available.png" ,files: []};
  }
  return {
    menus,
    getMenus,
    dialog,
    editedMenu,
    saveMenu,
    editMenu,
    deleteMenu,
    switchMenu,
    deleteDialog,
    menu,
    getMenusByName,
    search
  };
});
