import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Serve from "@/types/Serve";
import serveService from "@/services/serve";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useServeStore = defineStore("Serve", () => {
  // const loadingStore = useLoadingStore();
  // const messageStore = useMessageStore();
  // const dialog = ref(false);
  // const serve = ref<Serve[]>([]);
  // const editedServe = ref<Serve>({id: 0,status: ""} );

  // async function getServe() {
  //   loadingStore.isLoading = true;
  //   try {
  //     const res = await serveService.getServes();
  //     serve.value = res.data;
  //   } catch (e) {
  //     console.log(e);
  //     messageStore.showError("ไม่สามารถดึงข้อมูล Serve ได้");
  //   }
  //   loadingStore.isLoading = false;
  // }
  // async function deleteServe(id: number) {
  //   loadingStore.isLoading = true;
  //   try {
  //     const res = await serveService.deleteServe(id);
  //     await getServe();
  //   } catch (e) {
  //     console.log(e);
  //     messageStore.showError("ไม่สามารถลบข้อมูล product ได้");
  //   }
  //   loadingStore.isLoading = false;
  // }
  // async function saveServe() {
  //   loadingStore.isLoading = true;
  //   try {
  //     if (editedServe.value.id) {
  //       const res = await serveService.updateServe(
  //         editedServe.value.id,
  //         editedServe.value
  //       );
  //     } else {
  //       const res = await serveService.saveServe(editedServe.value);
  //     }

  //     dialog.value = false;
  //     // clearProduct();
  //     await getServe();
  //   } catch (e) {
  //     messageStore.showError("ไม่สามารถบันทึกข้อมูล product ได้");
  //     console.log(e);
  //   }
  //   loadingStore.isLoading = false;
  // }

  // function editServe(serve: Serve) {
  //   editedServe.value = JSON.parse(JSON.stringify(serve));
  //   dialog.value = true;
  // }
  return {
    // getServe,
    // serve,
    // deleteServe,
    // editServe,
    // editedServe,
    // saveServe,

  };
});
