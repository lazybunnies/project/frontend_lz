import { ref, watch } from "vue";
import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Table from "../types/ListTable";
import tableService from "@/services/list_table";
import orderService from "@/services/order";
import axios from "@/services/axios";
import type Order from "@/types/Order";
import type OrderUpdate from "@/types/OrderUpdate";

export const useTableStore = defineStore("Table", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const tables = ref<Table[]>([]);
  const editedTable = ref<Table>({ type: "", Number: 0, status: "" });
  const newTable = ref<Table>({ type: "", Number: 0, status: "" });
  const editedOrder = ref<OrderUpdate>({ status: "กำลังใช้งาน", table_id: 0 });
  const newOrder = ref<OrderUpdate>({ status: "กำลังใช้งาน", table_id: 0 });
  const order = ref<Order[]>([])
  const orderId = ref(0)
  const uuid = ref("")
  const qrCodeUrl = ref("");
  // const baseUrl = "http://lazybunnies.ddns.net"
  const baseUrl = "http://localhost"

  async function getOrderByTable(id: number) {
    try {
      const res = await orderService.getOrderBytable(id);
      order.value = res.data;
      for (const item of order.value) {
        orderId.value = item.id!;
        uuid.value = item.uuid!;
      }
    } catch (e) {
      console.log(e);
    }
    generateQRCode(orderId.value)
  }


  function generateQRCode(order: number) {
    qrCodeUrl.value = '' + baseUrl + '/food?order=' + uuid.value;
  }


  const ItemDialog = ref<Table>({
    id: -1,
    type: "",
    Number: 0,
    status: "",
  });

  const ItemChangeTable = ref<Table>({
    id: 0,
    type: "",
    Number: 0,
    status: "",
  });

  const changeTableStatus = () => {
    if (ItemDialog.value.status === "yellow") {
      const index = tables.value.findIndex(
        (item) =>
          item.id === ItemDialog.value.id && item.type === ItemDialog.value.type
      );
      tables.value[index].status = "green";
    } else if (ItemDialog.value.status === "red") {
      const index = tables.value.findIndex(
        (item) =>
          item.id === ItemDialog.value.id && item.type === ItemDialog.value.type
      );
      tables.value[index].status = "yellow";
    } else if (ItemDialog.value.status === "green") {
      const index = tables.value.findIndex(
        (item) =>
          item.id === ItemDialog.value.id && item.type === ItemDialog.value.type
      );
      tables.value[index].status = "red";
    }

    dialog.value = false;
  };

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedTable.value = { type: "", Number: 0, status: "" };
    }
  });
  const showTableName = (tables: Table) => {
    ItemDialog.value.id = tables.id;
    ItemDialog.value.type = tables.type;
    ItemDialog.value.Number = tables.Number;
    ItemDialog.value.status = tables.status;
  };

  async function getTable() {
    // loadingStore.isLoading = true;
    try {
      const res = await tableService.getTable();
      tables.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล List Table ได้");
    }
    // loadingStore.isLoading = false;
  }

  const Additem = (tables: Table) => {
    ItemDialog.value.id = tables.id;
    ItemDialog.value.type = tables.type;
    ItemDialog.value.Number = tables.Number;
    ItemDialog.value.status = tables.status;
    dialog.value = true;
    console.log(ItemDialog);
  };

  async function saveTable() {
    loadingStore.isLoading = true;
    try {
      if (editedTable.value.id) {
        const res = await tableService.updateTable(
          editedTable.value.id,
          editedTable.value
        );
      }
      else {
        const res = await tableService.saveTable(editedTable.value);
      }
      dialog.value = false;
      await getTable();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก List Table ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function OpenOrder() {
    loadingStore.isLoading = true;
    try {
      if (editedOrder.value.id) {
        const res = await orderService.updateOrder(
          editedOrder.value.id,
          editedOrder.value
        );
      }
      else {
        const res = await orderService.saveOrder(editedOrder.value);
      }
      dialog.value = false;
    } catch (e) {
      messageStore.showError("ไม่สามารถเปิด Order ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }


  function changeStatusTable() {
    editedTable.value = ItemDialog.value;
    editedTable.value.status = 'red'
    editedOrder.value.table_id = ItemDialog.value.id!;
    OpenOrder();
    saveTable();
  }


  const redTableStatus = () => {
    console.log("พาย");
    const index = tables.value.findIndex(
      (item) =>
        item.id === ItemDialog.value.id && item.type === ItemDialog.value.type
    );
    tables.value[index].status = "red";
  };

  async function deleteTable(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await tableService.deleteTable(id);
      await getTable();
    } catch (e) {
      messageStore.showError("ไม่สามารถลบข้อมูล List Table ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  // async function newOrder() {
  //   loadingStore.isLoading = true;
  //   try {
  //      // Create a new order ID
  //   const nextOrderIdRes = await axios.get('http://localhost:5173/orders/next-order-id');
  //   const nextOrderId = nextOrderIdRes.data.nextOrderId;

  //   // Save the new order
  //   const res = await orderService.saveOrder({ id: nextOrderId });

  //   dialog.value = false;
  //   await getOrder();
  //   } catch (e) {
  //     messageStore.showError("ไม่สามารถบันทึก Order ได้");
  //     console.log(e);
  //   }
  //   loadingStore.isLoading = false;
  // }

  function editTable(table: Table) {
    editedTable.value = JSON.parse(JSON.stringify(table));
    dialog.value = true;
  }
  const changeTable = (tables: Table) => {
    ItemChangeTable.value.id = tables.id;
    ItemChangeTable.value.type = tables.type;
    ItemChangeTable.value.Number = tables.Number;
    ItemChangeTable.value.status = tables.status;
  };

  const confirmChange = async () => {
    if (ItemDialog.value.status === "red") {
      editedTable.value.id = ItemDialog.value.id;
      editedTable.value = ItemDialog.value;
      editedTable.value.status = 'yellow'
      console.log(ItemDialog.value.id);
      saveTable();
    }

    getOrderByTable(ItemDialog.value.id!);
    if (ItemChangeTable.value.status === "green") {
      editedTable.value = ItemChangeTable.value;
      editedTable.value.status = 'red'
      console.log(ItemChangeTable.value.id);
      editedOrder.value.id = orderId.value;
      editedOrder.value.table_id = ItemChangeTable.value.id!;
      if(editedOrder.value.id){
        const res = await orderService.updateOrder(
          editedOrder.value.id,
          editedOrder.value
        );
      }
      saveTable();
    }
    ItemChangeTable.value = ({
      id: 0,
      type: "",
      Number: 0,
      status: "",
    });
  };
  return { changeStatusTable, qrCodeUrl, orderId, getOrderByTable, tables, getTable, dialog, ItemDialog, editedTable, ItemChangeTable, saveTable, deleteTable, editTable, Additem, changeTable, showTableName, changeTableStatus, redTableStatus, confirmChange };
});