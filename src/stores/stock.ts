import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Stock from "@/types/Stock";
import stockService from "@/services/stock";
import checkStockService from "@/services/checkStock";
import stockBuyService from "@/services/stockBuy";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type CheckStock from "@/types/CheckStock";
import type StockBuy from "@/types/StockBuy";
import type CheckStockList from "@/types/CheckStockList";
import type StockBuyList from "@/types/StockBuyList";

export const useStockStore = defineStore("Stock", () => {
    const userString = localStorage.getItem("user");
    const user = userString ? JSON.parse(userString) : null;
    const loadingStore = useLoadingStore();
    const messageStore = useMessageStore();
    const dialog = ref(false);
    const dialogCheck = ref(false);
    const dialogBuy = ref(false);
    const dialogDetail = ref(false);
    const deleteDialog = ref(false);
    const search = ref("");
    const searchStockList = ref("");
    const dID = ref();
    const dName = ref();

    const checkedStockListID = ref();
    const stockBuyListID = ref();
    const stocks = ref<Stock[]>([]);
    const checkStocks = ref<CheckStock[]>([]);
    const checkStockDetails = ref<CheckStock[]>([]);
    const stocksBuy = ref<StockBuy[]>([]);
    const stockBuyDetails = ref<StockBuy[]>([]);
    const checkStockDetailName = ref('');
    const checkStockDetailDate = ref('');
    const stockBuyDetailName = ref('');
    const stockBuyDetailDate = ref('');
    const checkStockLists = ref<CheckStockList[]>([]);
    const stockBuyLists = ref<StockBuyList[]>([]);
    const editedStock = ref<Stock>({ name: "", minimum: 0, remaining: 0, unit: "", price: 0 });
    const checkedStock = ref<CheckStock>({
        name: "",
        remaining: 0,
        unit: "",
        price: 0,
        check: 0,
    });

    const stockBuyed = ref<StockBuy>({
        name: "",
        store: "",
        amount: 0,
        unit: "",
        totalPrice: 0,
    });

    const checkedStockList = ref<CheckStockList>({});
    const stockBuyedList = ref<StockBuyList>({});
    const newIdCheckStockDetail = ref(0);

    function dialogDelete(item: Stock) {
        dID.value = item.id;
        dName.value = item.name;
        deleteDialog.value = true
    }

    watch(dialog, (newDialog, oldDialog) => {
        console.log(newDialog);
        if (!newDialog) {
            editedStock.value = { name: "", minimum: 0, remaining: 0, unit: "", price: 0 };
        }
    });

    watch(dialogCheck, (newDialog) => {
        console.log(newDialog);
        if (!newDialog) {
            checkedStock.value = {
                name: "",
                remaining: 0,
                unit: "",
                price: 0,
                check: 0,
            };
        }
    });

    watch(dialogBuy, (newDialog) => {
        console.log(newDialog);
        if (!newDialog) {
            stockBuyed.value = {
                name: "",
                store: "",
                amount: 0,
                unit: "",
                totalPrice: 0,
            };
        }
    });

    // watch(dialogDetail, (newDialog) => {
    //     console.log(newDialog);
    //     if (!newDialog) {
    //         checkedStock.value = { name: "",
    //         minimum: 0,
    //         remaining: 0,
    //         unit: "",
    //         price: 0,
    //         check: 0,
    //         user: user.id, };
    //     }
    // });
    async function getStockDetails(item: CheckStockList) {
        // loadingStore.isLoading = true;
        try {
            checkStockDetailName.value = item.user?.fname + ' ' + item.user?.lname;
            checkStockDetailDate.value = new Date(item.createdAt!).toLocaleDateString('af-ZA');
            const res = await checkStockService.getStockkDetail(item.id!)
            checkStockDetails.value = res.data;
            // if (searchStockList.value == '') {
            //     const res = await checkStockService.getCheckStockList();
            //     checkStocks.value = res.data;
            // } else {
            //     const res = await checkStockService.getCheckStockByDate(searchStockList.value);
            //     checkStocks.value = res.data;
            // }
        } catch (e) {
            console.log(e);
            messageStore.showError("ไม่สามารถดึงข้อมูล Stock ได้");
        }
        dialogCheck.value = true;
        // loadingStore.isLoading = false;
    }

    async function getStockDetails2(item: StockBuyList) {
        // loadingStore.isLoading = true;
        try {
            stockBuyDetailName.value = item.user?.fname + ' ' + item.user?.lname;
            stockBuyDetailDate.value = new Date(item.createdAt!).toLocaleDateString('af-ZA');
            const res = await stockBuyService.getStockBuyDetail(item.id!)
            console.log(res.data)
            stockBuyDetails.value = res.data;
            // if (searchStockList.value == '') {
            //     const res = await checkStockService.getCheckStockList();
            //     checkStocks.value = res.data;
            // } else {
            //     const res = await checkStockService.getCheckStockByDate(searchStockList.value);
            //     checkStocks.value = res.data;
            // }
        } catch (e) {
            console.log(e);
            messageStore.showError("ไม่สามารถดึงข้อมูล Stock ได้");
        }
        dialogBuy.value = true;
        // loadingStore.isLoading = false;
    }

    async function getStockBuyDetails(item: StockBuyList) {
        try {
            stockBuyDetailName.value = item.user?.fname + ' ' + item.user?.lname;
            stockBuyDetailDate.value = new Date(item.createdAt!).toLocaleDateString('th-TH');
            const res = await stockBuyService.getStockBuyDetail(item.id!)
            stockBuyDetails.value = res.data;
        } catch (e) {
            console.log(e);
            messageStore.showError("ไม่สามารถดึงข้อมูล Stock ได้");
        }
        dialogCheck.value = true;
    }

    async function getStocks() {
        // loadingStore.isLoading = true;
        try {
            if (search.value == '') {
                const res = await stockService.getStock();
                stocks.value = res.data;
            } else {
                const res = await stockService.getStockByName(search.value);
                stocks.value = res.data;
            }
            // const res = await checkStockService.getCheckStockList()
            // checkStockLists.value = res.data;
            const res1 = await stockBuyService.getStockBuyList()
            stockBuyLists.value = res1.data;
        } catch (e) {
            console.log(e);
            messageStore.showError("ไม่สามารถดึงข้อมูล Stock ได้");
        }
        // loadingStore.isLoading = false;
        if (checkStocks.value.length == 0) {
            for (const item of stocks.value) {
                checkStocks.value.push(item)
            }
        } else if (stocksBuy.value.length == 0) {
            for (const item of stocks.value) {
                const { name, price , unit,id } = item;
                const newItem = { name, price,unit,id };
                stocksBuy.value.push(newItem);
            }
        }
    }

    async function getStockLists() {
        try {
            const res = await checkStockService.getCheckStockList()
            checkStockLists.value = res.data;
        } catch (e) {
            console.log(e);
            messageStore.showError("ไม่สามารถดึงข้อมูล Stock ได้");
        }
    }

    async function getCheckStockLists(createAt: string) {
        try {
            const res = await checkStockService.getCheckStockByDate(createAt);
            checkStockLists.value = res.data;
        } catch (e) {
            console.log(e);
            messageStore.showError("ไม่สามารถดึงข้อมูล Stock ได้");
        }
    }

    async function getStockBuyList(createAt: string) {
        try {
            const res = await stockBuyService.getStockBuyByDate(createAt);
            stockBuyLists.value = res.data;
        } catch (e) {
            console.log(e);
            messageStore.showError("ไม่สามารถดึงข้อมูล Stock ได้");
        }
    }

    async function getStocksByName(name: string) {
        try {
            const res = await stockService.getStockByName(name);
            stocks.value = res.data;
        } catch (e) {
            console.log(e);
            messageStore.showError("ไม่สามารถดึงข้อมูล Stock ได้");
        }
    }

    async function getStocksByDate(createAt: string) {
        // try {
        //     const res = await checkStockService.getCheckStockByDate(createAt?);
        //     checkStockLists.value = res.data;
        // } catch (e) {
        //     console.log(e);
        //     messageStore.showError("ไม่สามารถดึงข้อมูล Stock ได้");
        // }
    }

    async function saveStock() {
        loadingStore.isLoading = true;
        try {
            if (editedStock.value.id) {
                const res = await stockService.updateStock(
                    editedStock.value.id,
                    editedStock.value
                );
            } else {
                const res = await stockService.saveStock(editedStock.value);
            }

            dialog.value = false;
            await getStocks();
        } catch (e) {
            messageStore.showError("ไม่สามารถบันทึก Stock ได้");
            console.log(e);
        }
        loadingStore.isLoading = false;
    }

    async function saveCheckStockList() {
        loadingStore.isLoading = true;
        try {
            checkedStockList.value.user = user.id;
            const res = await checkStockService.saveCheckStockList(checkedStockList.value);
            checkedStockListID.value = res.data.id;
            for (let i = 0; i < checkStocks.value.length; i++) {
                editedStock.value = { name: undefined, minimum: undefined, remaining: undefined, unit: undefined, price: undefined };
                editedStock.value.id = checkStocks.value[i].id
                editedStock.value.remaining = checkStocks.value[i].check!;
                const res1 = await stockService.updateStock(
                    editedStock.value.id!,
                    editedStock.value
                );
                checkStocks.value[i].id = undefined;
                checkStocks.value[i].checkStockList = checkedStockListID.value;
                const res2 = await checkStockService.saveCheckStockDetail(checkStocks.value[i]);

            }
            dialogCheck.value = false;
            // await getCheckStocks();
        } catch (e) {
            messageStore.showError("ไม่สามารถบันทึก Check Stock ได้");
            console.log(e);
        }
        loadingStore.isLoading = false;
    }

    async function saveStockBuyList() {

        loadingStore.isLoading = true;
        try {
            stockBuyedList.value.user = user.id;
            let sumtotalPrice = 0;
            for (let i = 0; i < stocksBuy.value.length; i++) {
                const totalPrice = stocksBuy.value[i].amount! * stocksBuy.value[i].price!;
                stocksBuy.value[i].totalPrice! = totalPrice;
                sumtotalPrice += stocksBuy.value[i].totalPrice!;
                stockBuyedList.value.totalPrice! = sumtotalPrice;
            }
            const res = await stockBuyService.saveStockBuyList(stockBuyedList.value);
            stockBuyListID.value = res.data.id;
            for (let i = 0; i < stocksBuy.value.length; i++) {
                editedStock.value = { name: undefined, minimum: undefined, remaining: undefined, unit: undefined, price: undefined };
                editedStock.value.id = stocksBuy.value[i].id
                editedStock.value.remaining = stocksBuy.value[i].amount!;
                console.log(editedStock.value)
                const res1 = await stockService.updateStock(
                    editedStock.value.id!,
                    editedStock.value
                );
                stocksBuy.value[i].id = undefined;
                stocksBuy.value[i].stockBuyList = stockBuyListID.value;
                console.log(stocksBuy.value[i])
                const res2 = await stockBuyService.saveStockBuyDetail(stocksBuy.value[i]);

            }
            dialogCheck.value = false;
            // await getCheckStocks();
        } catch (e) {
            messageStore.showError("ไม่สามารถบันทึก Stock Buy ได้");
            console.log(e);
        }
        loadingStore.isLoading = false;
    }

    // async function saveCheckStockDetail() {
    //     loadingStore.isLoading = true;
    //     try {
    //         if (checkedStock.value.id) {
    //             const res = await checkStockService.updateCheckStock(
    //                 checkedStock.value.id,
    //                 checkedStock.value
    //             );
    //         } else {

    //         }
    //         // dialogCheck.value = false;
    //         // await getCheckStocks();
    //     } catch (e) {
    //         messageStore.showError("ไม่สามารถบันทึกได้");
    //         console.log(e);
    //     }
    //     loadingStore.isLoading = false;
    // }

    async function deleteStock(id: number) {
        loadingStore.isLoading = true;
        try {
            const res = await stockService.deleteStock(id);
            await getStocks();
        } catch (e) {
            console.log(e);
            messageStore.showError("ไม่สามารถลบ Stock ได้");
        }
        deleteDialog.value = false;
        loadingStore.isLoading = false;
    }

    function editStock(stock: Stock) {
        editedStock.value = JSON.parse(JSON.stringify(stock));
        dialog.value = true;
    }

    function checkStock(checkStock: CheckStock) {
        checkedStock.value = JSON.parse(JSON.stringify(checkStock));
        dialogCheck.value = true;
    }

    function stockBuy(stockBuy: StockBuy) {
        stockBuyed.value = JSON.parse(JSON.stringify(stockBuy));
        dialogBuy.value = true;
    }

    return {
        stocks,
        checkStocks,
        stocksBuy,
        getStocks,
        dialog,
        dialogCheck,
        dialogBuy,
        editedStock,
        dialogDetail,
        saveStock,
        editStock,
        deleteStock,
        deleteDialog,
        dialogDelete,
        dID,
        dName,
        getStocksByName,
        getStockBuyList,
        getStockDetails2,
        stockBuyLists,
        stockBuyDetails,
        stockBuyDetailDate,
        stockBuyDetailName,
        search,
        checkedStock,
        checkStock,
        checkStockLists,
        checkedStockList,
        saveCheckStockList,
        getStockDetails,
        checkStockDetails,
        checkStockDetailName,
        checkStockDetailDate,
        getCheckStockLists,
        searchStockList,
        getStocksByDate,
        getStockBuyDetails,
        saveStockBuyList,
        stockBuy,
        getStockLists,
    };
});
