import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Table from "../types/ListTable";
import tableService from "@/services/list_table";
import paymentService from "@/services/payment";
import orderitemService from "@/services/orderitem"
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useMenuStore } from "./menu";
import type Payment from "@/types/Payment";
import { useFoodStore } from "./food";
import orderService from "@/services/order";
import type Order from "@/types/Order";
import type Orderitem from "@/types/Orderitem";
import type OrderUpdate from "@/types/OrderUpdate";
import type PaymentAdd from "@/types/PaymentAdd";


export const usePaymentStore = defineStore("table", () => {
  //const menuStore = useMenuStore();
  const userString = localStorage.getItem("user");
  const user = userString ? JSON.parse(userString) : null;
  const foodStore = useFoodStore();
  const sum = ref(0);
  const table = ref('??');
  const dialog = ref(false);
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const tables = ref<Table[]>([]);
  const editedTable = ref<Table>({ type: "", Number: 0, status: "" });
  const editedOrder = ref<OrderUpdate>({ status: "ชำระเงินแล้ว", table_id: 0 });
  const confirmList = ref<Orderitem[]>([]);
  const order = ref<Order[]>([])
  const orderId = ref(0)
  const drawer = ref(false);
  const tableId = ref(0);
  const editedPay = ref<PaymentAdd>({
    price: 0,
    received: 0,
    moneyChange: 0,
    netPrice: 0,
    discount: 0,
    order: 0,
    user: user.id,
  })

  const PayOrder = ref<Payment[]>([{
    price: 0,
    received: 0,
    moneyChange: 0,
    netPrice: 0,
    discount: 0,
  }]);



  async function getOrderByTable(id: number) {
    try {
      const res = await orderService.getOrderBytable(id);
      order.value = res.data;
      for (const item of order.value) {
        orderId.value = item.id!;
        tableId.value = item.table_id.id!;
      }
    } catch (e) {
      console.log(e);
    }
    foodStore.getOrderCF('' + orderId.value)
    drawer.value = true;
  }

  async function updateOrder() {
    if (editedOrder.value.id) {
      const res = await orderService.updateOrder(
        editedOrder.value.id,
        editedOrder.value
      );
    }
  }

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedTable.value = { type: "", Number: 0, status: "" };
    }
  });

  const ItemDialog = ref<Table>({
    id: -1,
    type: "",
    Number: 0,
    status: "",
  });

  const ItemChangeTable = ref<Table>({
    id: 0,
    type: "",
    Number: 0,
    status: "",
  });

  const Additem = (tables: Table) => {
    ItemDialog.value.id = tables.id;
    ItemDialog.value.type = tables.type;
    ItemDialog.value.Number = tables.Number;
    ItemDialog.value.status = tables.status;
    dialog.value = true;
  };

  async function getTable() {
    try {
      const res = await tableService.getTable();
      tables.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล List Table ได้");
    }
  }

  async function saveTable() {
    try {
      if (ItemDialog.value.id) {
        const res = await tableService.updateTable(
          ItemDialog.value.id,
          ItemDialog.value
        );
      }
      else {
        const res = await tableService.saveTable(ItemDialog.value);
      }
      dialog.value = false;
      await getTable();
    } catch (e) {
      messageStore.showError("ไม่สามารถ Update สถานะโต๊ะได้");
      console.log(e);
    }
  }

  async function changeTableStatus() {
    if (ItemDialog.value.status == 'red') {
      ItemDialog.value.status = 'yellow'
    }
    saveTable();
  }
  const confirmChange = () => {
    if (ItemDialog.value.status === "red") {
      const index = tables.value.findIndex(
        (item) =>
          item.id === ItemDialog.value.id && item.type === ItemDialog.value.type
      );
      tables.value[index].status = "yellow";
    }

    if (ItemChangeTable.value.status === "green") {
      const index = tables.value.findIndex(
        (item) =>
          item.id === ItemChangeTable.value.id &&
          item.type === ItemChangeTable.value.type
      );
      tables.value[index].status = "red";
    }
  };

  async function savePayment() {
    try {
      if (editedPay.value.id) {
        const res = await paymentService.updatePayment(
          editedPay.value.id,
          editedPay.value
        );
      }
      else {
        console.log(editedPay.value);
        const res = await paymentService.savePayment(editedPay.value);
      }
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Payment ได้");
      console.log(e);
    }
  }

  function sumprice() {
    sum.value = 0;
    for (const item of confirmList.value) {
      sum.value += item.amount * item.menu.price
      table.value = item.order.table_id.type + item.order.table_id.Number;
    }
  }

  async function getOrderCF(order: string) {
    try {
      const res = await orderitemService.getBasketByOrder(order);
      confirmList.value = res.data;
    } catch (e) {
      console.log(e);
    }
    sumprice();
  }
  return { drawer, dialog, Additem, getTable, changeTableStatus, ItemDialog, PayOrder, confirmChange, getOrderByTable, order, orderId, sum, getOrderCF, updateOrder, editedOrder, tableId, editedPay, savePayment };
});