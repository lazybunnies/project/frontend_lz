import { ref } from "vue";
import { defineStore } from "pinia";
import type Table from "../types/ListTable";
import tableService from "@/services/list_table";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";


export const useCleanStore = defineStore("Clean", () => {
    // const lastId = 5;
    const loadingStore = useLoadingStore();
    const messageStore = useMessageStore();
    const ItemDialog = ref<Table>({
        id: -1,
        type: "",
        Number: 0,
        status: "",
    });

    const ItemChangeTable = ref<Table>({
        id: 0,
        type: "",
        Number: 0,
        status: "",
    });

    const tables = ref<Table[]>([]);

    const dialog = ref(false);

    const Additem = (tables: Table) => {
        ItemDialog.value.id = tables.id;
        ItemDialog.value.type = tables.type;
        ItemDialog.value.Number = tables.Number;
        ItemDialog.value.status = tables.status;
        dialog.value = true;
    };

    async function getTable() {

        try {
            const res = await tableService.getTable();
            tables.value = res.data;
        } catch (e) {
            console.log(e);
            messageStore.showError("ไม่สามารถ Update สถานะโต๊ะได้");
        }

    }

    async function saveTable() {
       
        try {
            if (ItemDialog.value.id) {
                const res = await tableService.updateTable(
                    ItemDialog.value.id,
                    ItemDialog.value
                );
            }
            else {
                const res = await tableService.saveTable(ItemDialog.value);
            }
            dialog.value = false;
            await getTable();
        } catch (e) {
            messageStore.showError("ไม่สามารถ Update สถานะโต๊ะได้");
            console.log(e);
        }
        
    }

    async function changeTableStatus() {
        if (ItemDialog.value.status == 'yellow') {
            ItemDialog.value.status = 'green'
        }
        saveTable();
    }


    const showTableName = (tables: Table) => {
        ItemDialog.value.id = tables.id;
        ItemDialog.value.type = tables.type;
        ItemDialog.value.status = tables.status;
    };

    const changeTable = (tables: Table) => {
        ItemChangeTable.value.id = tables.id;
        ItemChangeTable.value.type = tables.type;
        ItemChangeTable.value.Number = tables.Number;
        ItemChangeTable.value.status = tables.status;
    };

    const confirmChange = () => {
        if (ItemDialog.value.status === "red") {
            const index = tables.value.findIndex(
                (item) =>
                    item.id === ItemDialog.value.id && item.type === ItemDialog.value.type
            );
            tables.value[index].status = "yellow";
        }

        if (ItemChangeTable.value.status === "green") {
            const index = tables.value.findIndex(
                (item) =>
                    item.id === ItemChangeTable.value.id &&
                    item.type === ItemChangeTable.value.type
            );
            tables.value[index].status = "red";
        }
    };

    return {
        tables,
        Additem,
        dialog,
        ItemDialog,
        changeTableStatus,
        changeTable,
        showTableName,
        ItemChangeTable,
        confirmChange,
    };
});
