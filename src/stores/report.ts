import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import reportService from "@/services/report"
import type Orderitem from '@/types/Orderitem';
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';
import type Queue from '@/types/Queue';
import type Menu from '@/types/Menu';
import type TypeTop from '@/views/reports/TypeTop';
import type TypeChef from '@/views/reports/TypeChef';

export const useReportStore = defineStore('report', () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const sumprice = ref(0);
  const sumOrder = ref(0);
  const countTable = ref(0);
  const time = ref('day');
  const topMenu = ref<TypeTop[]>([]);
  const topChef = ref<TypeChef[]>([]);
  const topServe = ref<TypeChef[]>([]);
  const price7day = ref();
  const price7sum = ref();
  const graphName = ref('bar');
  const textGraph = ref('');

  function reset(){
    sumprice.value = 0;
    sumOrder.value = 0;
    countTable.value = 0;
  }

  async function getSumPrice(text:string) {
    try {
      const res = await reportService.sumPrice(text);
      if(res.data[0].sum_amount != null){
        sumprice.value = res.data[0].sum_amount;
      }
      return res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูลได้");
    }
  }

  async function getSumOrder(text:string) {
    try {
      const res = await reportService.sumOrder(text);
      if(res.data[0].sum_Order != null){
        sumOrder.value = res.data[0].sum_Order;
      }
      return res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูลได้");
    }
  }

  async function getCountTable(text:string) {
    try {
      const res = await reportService.countTable(text);
      countTable.value = res.data[0].count;
      return res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูลได้");
    }
  }

  async function getTopMenu(text:string) {
    try {
      const res = await reportService.topMenu(text);
      topMenu.value = res.data;
      return res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูลได้");
    }
  }

  async function getTopChef(text:string) {
    try {
      const res = await reportService.topChef(text);
      topChef.value = res.data;
      return res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูลได้");
    }
  }

  async function getTopServe(text:string) {
    try {
      const res = await reportService.topServe(text);
      topServe.value = res.data;
      return res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูลได้");
    }
  }


  async function getprice() {
    try {
      if(time.value == 'day'){
        const res = await reportService.price7day();
        price7day.value = res.data.map((item: { day: string }) => item.day);
        price7sum.value = res.data.map((item: { sum: number }) => Number(item.sum));
        console.log(price7day.value)
        return res.data;
      }
      // else if(time.value == 'month'){
      //   const res = await reportService.price12month();
      //   price7day.value = res.data.map((item: { month: string }) => item.month);
      //   price7sum.value = res.data.map((item: { sum: number }) => Number(item.sum));
      //   return res.data;
      // }
      
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูลได้");
    }
  }



  




  return { reset,sumprice, getSumPrice ,getSumOrder ,sumOrder,getCountTable,countTable,getTopMenu,topMenu,time,getTopChef,topChef,topServe,getTopServe,getprice,price7day,price7sum,graphName}
})
